notes 

login at:
jupyter.gwdg.de with gwdg account

course material via terminal + git clone [url]

1 Torsten
2 Sayedeh
3 Johannes
4 Torsten
5 Sayedeh
6 Johannes
7 Torsten
8 Sayedeh
9 Johannes
(10) Sayedeh + Johannes

changes (to do):

order:
    * maybe bigger exercise? like compute/plot histogram of letter freqs.

exceptions
    * low prio: throw Exception("your input was wrong") as means of simple feedback
    

before course:
send reminder email (including: room, time, GWDG account, maybe try to use login?)
check the wifi access point in the room
check if presenter & other equipment works